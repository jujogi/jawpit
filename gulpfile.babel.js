import gulp from 'gulp';
import babel from 'gulp-babel';
import sass from 'gulp-sass';
import postcss from 'gulp-postcss';
import cssnano from 'cssnano';
import uglify from 'gulp-uglify';
import browserSync from 'browser-sync';
import concat from 'gulp-concat';
import htmlmin from 'gulp-htmlmin';

const server = browserSync.create(),
    postCSSPlugins = [
        cssnano({
            autoprefixer: {
                add: true
            }
        })
    ];

gulp.task('sass', () =>
    gulp.src('./src/scss/**.scss')
    .pipe(sass())
    .pipe(postcss(postCSSPlugins))
    .pipe(gulp.dest('./public/css'))
    .pipe(server.stream({
        match: '**/*.css'
    }))
);

gulp.task('js', () =>
    gulp.src('./src/js/*.js')
    .pipe(babel())
    .pipe(uglify())
    .pipe(gulp.dest('./public/js'))
);

gulp.task('minify', () => {

    return gulp.src('./src/js/*.js')
    .pipe(babel())
    .pipe(uglify())
    .pipe(concat('scripts.min.js'))
    .pipe(gulp.dest('./public/js'));
});

gulp.task('html', function() {
    return gulp.src('./public/structure/*.html')
      .pipe(htmlmin({collapseWhitespace: true}))
      .pipe(gulp.dest('./public/structure/dist'));
  });

gulp.task('default', () => {
    server.init({
        server: {
            baseDir: './public'
        }
    });
    gulp.watch('./src/scss/**/*.scss', ['sass']);
    gulp.watch('./src/js/*.js', ['js', server.reload]);
    gulp.watch("./public/components/**/*.html").on("change", server.reload);
    gulp.watch("./public/pages/**/*.html").on("change", server.reload);
    gulp.watch("./public/structure/**/*.html").on("change", server.reload);

});
