const grid = document.getElementById('grid')
const projects = document.getElementById('projects')


const executeGrid = ele => {

    ele.addEventListener('click', e => {
        if(e.target.className.includes('bepart-js')) e.target.closest('.grid__item').classList.add('active')
        if(e.target.className.includes('close-js')) e.target.closest('.grid__item').classList.remove('active')
    })

}

if (typeof(grid) != 'undefined' && grid != null) executeGrid(grid)
if (typeof(projects) != 'undefined' && projects != null) executeGrid(projects)
