const inizialitePrincipalSlider = () => {
  const swiper = new Swiper('.slider-container.swiper-container', {
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
  });
}

if (typeof (Swiper) != 'undefined' && Swiper != null) inizialitePrincipalSlider()