const burguer = document.getElementById("burger-icon"),
  menu = document.getElementById("menu__categories");


  const inizialiteToggleMenu = () => {


    burguer.addEventListener("click", e => {
      if (burguer.classList.contains("open")) {
        burguer.classList.remove("open");
        menu.classList.remove("open");
      } else {
        burguer.classList.add("open");
        menu.classList.add("open");
      }
    })
  }
  
if (typeof (burguer) != 'undefined' && burguer != null) inizialiteToggleMenu()

