let DOMvalue = document.getElementById('number')


const numberFormat = ele => {

    let value = parseInt(ele.dataset.amount)
    convertNumberToArray(value.toLocaleString())
}

const convertNumberToArray = number => {

    let arrayValue = []

    number.split("").map(number => {

        (number === ',') ?
        arrayValue.push(['text', number]): arrayValue.push(['number', number])

    })

    DrawInDOM(arrayValue)

}

const DrawInDOM = arr => {

    let group = '';

    for (let i = 0; i < arr.length; i++) {
        group += `<div class="${arr[i][0]}"> <span>${arr[i][1]}</span> </div>`
    }

    let amount = document.createElement('div')
    amount.id = 'amount'
    amount.innerHTML = group
    DOMvalue.appendChild(amount)


}

if (typeof (DOMvalue) != 'undefined' && DOMvalue != null) numberFormat(DOMvalue)



const inizialiteSharkSlide = () => {

    const own = new Swiper('.swiper-container.team', {
            slidesPerView: 5,
            spaceBetween: 5,
            // width: 600,
            breakpoints: {
                480: {
                    slidesPerView: 3
                },
                320: {
                    slidesPerView: 2
                }
            },
            navigation: {
                nextEl: '.join .swiper-button-next',
                prevEl: '.join .swiper-button-prev',
            }
        })
}

if (typeof (Swiper) != 'undefined' && Swiper != null) inizialiteSharkSlide()




