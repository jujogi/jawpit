const inizialiteSlides = () => {

    const own = new Swiper('.swiper-container.own-projects', {
            slidesPerView: 2,
            navigation: {
                nextEl: '.own-projects .swiper-button-next',
                prevEl: '.own-projects .swiper-button-prev',
            },
            breakpoints: {
                1060: {
                    slidesPerView: 3
                },
                667: {
                    slidesPerView: 2
                },
                500: {
                    slidesPerView: 1
                }
            }
        }),
        projects = new Swiper('.swiper-container.following-projects', {
            slidesPerView: 2,
            spaceBetween: 5,
            autoHeight: true,
            breakpoints: {
                500: {
                    slidesPerView: 1
                }
            },
            navigation: {
                nextEl: '.following-projects .swiper-button-next',
                prevEl: '.following-projects .swiper-button-prev',
            }
        })

}

if (typeof (Swiper) != 'undefined' && Swiper != null) inizialiteSlides()